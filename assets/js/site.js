$(document).ready(function(){
  var $status = $('.pagingInfo');
  var $slickElement = $('.slider');

  $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
      //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
      var i = (currentSlide ? currentSlide : 0) + 1;
      $status.text(i + '/' + slick.slideCount);
  });

  $(".slider").slick({
  dots: false,
  slidesToShow: 1,
  autoplay: true,
  autoplaySpeed: 3000,
  centerMode: true,
  centerPadding: '0px',
  prevArrow: $('.slide-prev'),
  nextArrow: $('.slide-next'),
  customPaging : function(slider, i) {
  var thumb = $(slider.$slides[i]).data();
  return '<a>1</a>';
          }
  });
});
